<div class="article1">
	<div class="article1-block">
		<div class="title1">
			海天“晒“宴，和汪涵一起，参观海天酱油阳光工厂
		</div>
		<div class="data">
			<div>
				<p class = 'p1'>
				    <i class = 'love'></i>
	   	    		<span class = 'love-num'>120</span>
	   	    		<i class = 'see'></i>
	   	    		<span class = 'visit'>560</span>
				</p>
			</div>
			<div>
				<p class = 'p2'>
				<span>2015-1-20</span>
				<span class = 'red'>[数字营销]</span>
			</p>
			</div>
		</div>
		<div class="content1 content word-indent">
			摘要：“海天阳光之旅”，携手汪涵启程啦！ 幸运的你，将与汪涵近距离互动，一起参观海天酱油阳光工厂，探秘超大规模调味王国！三天两夜免费体验岭南风情，夜游珠江登高小蛮腰，畅游长隆品粤式美食！还等什么？赶快参与互动，报名参加“海天阳光之旅”吧！
		</div>
		<div class="title2 word-indent">
			互联网呈现移动化、社交化、视频化等形式，未来互联网营销是新趋向。
		</div>
		<div class="content2 content word-indent">
			与此同时，随着互联网的移动化快速发展与社会化趋势，网民需求正在逐渐多样化。在这一前提下，无论是互联网巨头还是各种创新型互联网企业，单凭自身的资源与创新能力已经无法满足依赖移动互联网平台，表达形式日渐多样化的网民日益多元化的需求。有鉴于此，海天在2014年开始大力发展互联网营销事业，试图在互联网抢夺更多的用户。
		</div>
		<div class="title2 word-indent">
			  剑指对手，紧追猛打—海天酱油抢回“晒”语权
		</div>
		<div class="content3 content word-indent">
			直接剑指对手，打出新口号，紧追猛打。针对对手的产品口号，2014年海天酱油新定位—晒出来的好酱油，不仅晒足180天哦！在对手已经教育消费者，好酱油是晒出来的层面上，继续强势教育消费者，其实好酱油不止晒足180天，进一步加深消费者的印象，强势植入品牌概念。希望在新的领域抢夺更多的用户，挖掘更多的潜在受众。
		</div>
		<div class="img img1">
			<img src="/imgs/a1-1.png">
		</div>
		<div class="title2 word-indent">
		    全网覆盖，最大声量--海天在新阵地上刷新年轻新形象
		 </div>
		<div class="content4 content word-indent">
			    <p>
			    互联网一直是年轻人的聚集地，想要扩大活动声量
				结合人群属性和触媒习惯，选择在互联网作为主阵地，最大力度地扩散活动的影响力，聚焦主流平台及优势资源，引爆海天在互 联网上的传播力，为品牌活动引流。
			    </p>
			    <p> 四大门户媒体：品牌及活动主力支撑平台，为品牌做公关护航</p>
			    <p> 四大视频媒体：品牌TVC补充曝光，为活动引流助力社会化媒体自发扩散，通过大量转发，促进二次传播。</p>
		</div>
		<div class="img img2">
			<img src="/imgs/a1-2.png">
		</div>
		<div class="title2 word-indent">策略方向</div>
		<div class="content content5 word-indent">
			  点面结合，互动为王
		</div>

		<div class="title2 word-indent">点</div>
		<div class="content content6 word-indent">紧贴明星汪涵话题点，借助汪涵的明星效应，紧跟工厂游社会热点，通过参观工厂，探秘工厂内部制作酱油的全过程紧抓网友利益点：设置互动活动奖励机制，刺激网友参加
</div>

		<div class="title2 word-indent">面</div>
		<div class="content content7 word-indent">
			媒介公关全面造势：全方位扩散“海天阳光之旅”的活动精准视频渗透：重点推广品牌TVC，继续借助明星效应社会化口碑广泛积累：通过口口相传的口碑营销，为活动积累人气
		</div>
		<div class="img img3">
			<img src="/imgs/a1-3.png">
		</div>
		<div class="title2">活动简介：</div>
		<div class="content content8">
			<p>
				 “海天阳光之旅”，携手汪涵启程啦！
			</p>
			 <br/>
            <p>
            	幸运的你，将与汪涵近距离互动，一起参观海天酱油阳光工厂，探秘超大规模调味王国！三天两夜免费体验岭南风情，夜游珠江登高小蛮腰，畅游长隆品粤式美食！还等什么？赶快参与互动，报名参加“海天阳光之旅”吧！
            </p>
 
		</div>

		<div class="title2">活动时间：</div>
		<div class="content content9">
			<p>
				  2014年9月15日—2014年10月30日
			</p>
             <br/>
			<p>
				多版活动素材，增强活动新鲜感，提升网民参与热度！
			分为不同的时期，分别推出不同的网站页面，通过分阶段的视觉冲击，增加活动的趣味性，吸引更多的网友持续关注活动，为活动打造更强的互动体验。
			</p>
		</div>
		<div class="img img4">
			<img src="/imgs/a1-4.png">
		</div>
        <div class="content content9">
        	 趣味化广告 + 晒酱油视频 + 利益性抽奖,引爆全网最大化传播！
             借助汪涵的名人效应，不断地播放品牌TVC，强势教育消费者
             设置不同的奖项，刺激消费者带动更多的人参与活动，引发全网最大化传播
        </div>
        <div class="img img5">
			<img src="/imgs/a1-5.png">
		</div>
		<div class="content content10">
			<p>
			  截止10月13日，参与活动人数已达157万，海天引发全网互动狂潮！撬动网民内心支点奖品派不停！参与人数持续升高！
			</p>
			<br/>
			<p>
		    	20元红包1000份，共计2万元
			</p>
			<br/>
			<p>
			   50元红包500份，共计2万5千元
			</p>
			<br/>
			<p>
				合计已派发红包1500份！
			</p>
		</div>
		<div class="img img6">
			<img src="/imgs/a1-6.png">
		</div>
		<div class="content content11">
           总的来说，“阳光海天之旅”，集中资源，全网传播，互动效果明显，社会化传播上好评如潮对比厨邦酱油、李锦记，海天酱油在百度指数上遥遥领先，成功建立竞争壁垒！
		</div>
		<div class="img img7">
			<img src="/imgs/a1-7.png">
		</div>
	</div>
</div>


<div class="prev-page">
	<span class="title">上一篇：</span>
    <span class = 'text'>Flyme的新Logo也一同亮相</span>
</div>


<div class="operation">
	<ul>
		<li>
			<i class = 'love'></i>
			<span>点赞</span>
		</li>
		<li>
			<i class = 'collect'></i>
			<span>收藏</span>
		</li>
		<li>
			<i class = 'share'></i>
			<span>分享</span>
		</li>
	</ul>
	<div class="comment-form">
		<div class="form1">
			<p class = 'hide'>
				<a href="#">登录</a>
			    <span>后参与评论</span>
			</p>
			<p class = 'show'>
				<textarea></textarea>
			</p>
		</div>
		<p>
			<a href="#" class = 'publish-btn'>发布评论</a>
		</p>
	</div>
	<div class="commont">
		 <p class = 'commont-num'>
		 	<span>已有</span>
		 	<a href="#">3</a>
		 	<span>条评论</span>
		 </p>
		 <div class="commont-container">
		 	  <div class="commont-list">
		 	  	     <div class="commont-title">
		 	  	     	  <p class = 'l1'>
		 	  	     	  	  <span class = 'portrait'></span>
		 	  	     	  	  <span class = 'name'>YanXinHeng</span>
		 	  	     	  	  <span class = 'time'>1天前</span>
		 	  	     	  </p>
		 	  	     	  <p class = 'l2'>
		 	  	     	      <span class = 'text'>在这个互相仇恨的年代，抓的点不错</span>
		 	  	     	      <span class = 'right-time'>2015-11-8 11:04</span>
		 	  	     	  </p>
		 	  	     	  <p class = 'l3'>
		 	  	     	  	  <span class = 'num'>(2)</span>
		 	  	     	  	  <span class = 'text'>回复</span>
		 	  	     	  	  <span  class = 'icon'></span>
		 	  	     	  </p>
		 	  	     </div>
		 	  	     <div class="replay-container">
		 	  	     	   <div class="replay-list">
		 	  	     	   	    <p class = 'p1'>
		 	  	     	   	    	<span class = 'head'></span>
		 	  	     	   	    	<span class = 'name-cxt'>
		 	  	     	   	    		<span class="name">Mimo:</span>
		 	  	     	   	    	    <span class = 'cxt'>在这个互相仇恨的年代，抓的点不错在这个互相仇恨的年代，抓的点不错</span>
		 	  	     	   	    	</span>
		 	  	     	   	    </p>
		 	  	     	   	    <p class = 'replay-time'>2015-11-9 10:02</p>
		 	  	     	   </div>
		 	  	     	   
		 	  	     </div>
		 	  	     <div class="replay-form">
		 	  	     	  <div class="form2">
		 	  	     	  	<textarea></textarea>
		 	  	     	  	<p>3/50</p>
		 	  	     	  </div>
		 	  	     	  <div class = 'btn'>
		 	  	     	      <a href="replay-publish">发布</a>	
		 	  	     	  </div>
		 	  	     </div>
		 	  </div>
		 </div>
	</div>
</div>





<div class="get-more">
	 <a href="#">获取更多</a>
</div>

