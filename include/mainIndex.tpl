<div class="banner">
	<div class="logo1">
		<img src="/imgs/red-logo.png">
	</div>
	<div class="ali-logo">
		<img src="/imgs/alimama.png">
	</div>
</div>

<div class="main-nav">
	 <div class="nav">
	 	<img src="/imgs/main-nav.png">
	 </div>
	 <div class="mask">
	 	   <p class = 'line1'>省广股份携手阿里妈妈举行DT大时代 全息营销</p>
	 	   <p class = 'line2'>
	 	   	    <span class = 'left'>
	   	    		<i class = 'love'></i>
	   	    		<span class = 'love-num'>120</span>
	   	    		<i class = 'see'></i>
	   	    		<span class = 'visit'>560</span>
	   	    	</span>
	   	    	<span class="circle">
	   	    		 <i class = 'choose'></i>
			  	 	 <i></i>
			  	 	 <i></i>
			  	 	 <i></i>
			  	 	 <i></i>
	   	    	</span>
	   	    	<span class="right-time">
	   	    		12月26日 07:33:43
	   	    	</span>
	 	   </p>
	 </div>
</div>


<div class="main-type1">
	<div class="type1-list">
		 <div class="type1-left">
		 	<p class = 'line1'>
		 	     #苍井空#互联网卖内衣全球
				  引爆声量飙至7.9亿
		    </p>
		    <p class = 'line2'>
		    	<span class = 'love'>
		    		 <i></i>
		    	<span>120</span>
		    	</span>
		    	<span class = 'msg'>
		    		 <span class = 't1'>2小时前</span>
		    		 <span class = 't2'>[案例观点]</span>
		    	</span>
		    </p>
		 </div> 
		 <div class="right-img">
		 	 <img src="/imgs/m1.png">
		 </div>	
	</div>
	<div class="type1-list">
		 <div class="type1-left">
		 	<p class = 'line1'>
		 	     #苍井空#互联网卖内衣全球
				  引爆声量飙至7.9亿
		    </p>
		    <p class = 'line2'>
		    	<span class = 'love'>
		    		 <i></i>
		    		 <span>866</span>
		    	</span>
		    	<span class = 'msg'>
		    		 <span class = 't1'>5小时前</span>
		    		 <span class = 't2'>[课程]</span>
		    	</span>
		    </p>
		 </div> 
		 <div class="right-img">
		 	 <img src="/imgs/m2.png">
		 </div>	
	</div>
	<div class="clear"></div>
</div>


<div class="main-type2">
	 <div class="type2">
	 	 <div class="title">#苍井空#互联网卖内衣全球引爆声量飙至7.9亿</div>
		 <div class="img-list">
		 	 <div class="img">
		 	 	<img src="/imgs/m3.png">
		 	 </div>
		 	 <div class="img">
		 	 	<img src="/imgs/m4.png">
		 	 </div>
		 	 <div class="img">
		 	 	<img src="/imgs/m5.png">
		 	 </div>
		 	 <div class="clear"></div>
		 </div>
		 <div class="message">
		 	<div class="left">
		 		<span class = 'head'></span>
		 		<span class = 'name'>陈争光</span>
		 		<span class = 'com'>
		 			<i></i>
		 			<span>6868</span>
		 		</span>
		 		<span class = 'love'>
		 			<i></i>
		 			<span>4808</span>
		 		</span> 
		 	</div>
		 	<div class="right">
		 		<span class = 't1'>2天前</span>
		 		<span class = 't2'>[案例观点]</span>
		 	</div>
		 </div>
	 </div>
</div>



<div class="main-type3">
	<div class="type3-list">
		 <div class="type3-left">
		 	<p class = 'line1'>
		 	     #苍井空#互联网卖内衣全球
				  引爆声量飙至7.9亿
		    </p>
		    <p class = 'line2'>
		    	<span class = 'love'>
		    		 <i></i>
		    		 <span>120</span>
		    	</span>
		    	<span class = 'msg'>
		    		 <span class = 't1'>12月26日 07:33:43</span>
		    		 <span class = 't2'>[新闻]</span>
		    	</span>
		    </p>
		 </div> 
		 <div class="right-img">
		 	 <img src="/imgs/m6.png">
		 </div>	
	</div>
	<div class="type3-list">
		 <div class="type3-left">
		 	<p class = 'line1'>
		 	     #苍井空#互联网卖内衣全球
				  引爆声量飙至7.9亿
		    </p>
		    <p class = 'line2'>
		    	<span class = 'love'>
		    		 <i></i>
		    		 <span>866</span>
		    	</span>
		    	<span class = 'msg'>
		    		 <span class = 't1'>12月26日 07:33:43</span>
		    		 <span class = 't2'>[课程]</span>
		    	</span>
		    </p>
		 </div> 
		 <div class="right-img">
		 	 <img src="/imgs/m7.png">
		 </div>	
	</div>
</div>



<div class="main-type2">
	 <div class="type2">
	 	 <div class="title">#苍井空#互联网卖内衣全球引爆声量飙至7.9亿</div>
		 <div class="img-list">
		 	 <div class="img">
		 	 	<img src="/imgs/m8.png">
		 	 </div>
		 	 <div class="img">
		 	 	<img src="/imgs/m9.png">
		 	 </div>
		 	 <div class="img">
		 	 	<img src="/imgs/m10.png">
		 	 </div>
		 	 <div class="clear"></div>
		 </div>
		 <div class="message">
		 	<div class="left">
		 		<span class = 'head'></span>
		 		<span class = 'name'>陈争光</span>
		 		<span class = 'com'>
		 			<i></i>
		 			<span>6868</span>
		 		</span>
		 		<span class = 'love'>
		 			<i></i>
		 			<span>4808</span>
		 		</span> 
		 	</div>
		 	<div class="right">
		 		<span class = 't1'>2天前</span>
		 		<span class = 't2'>[案例观点]</span>
		 	</div>
		 </div>
	 </div>
</div>


<div class="main-type3">
	<div class="type1-list">
		 <div class="type1-left">
		 	<p class = 'line1'>
		 	     #苍井空#互联网卖内衣全球
				  引爆声量飙至7.9亿
		    </p>
		    <p class = 'line2'>
		    	<span class = 'love'>
		    		 <i></i>
		    		 <span>120</span>
		    	</span>
		    	<span class = 'msg'>
		    		 <span class = 't1'>12月26日 07:33:43</span>
		    		 <span class = 't2'>[新闻]</span>
		    	</span>
		    </p>
		 </div> 
		 <div class="right-img">
		 	 <img src="/imgs/m11.png">
		 </div>	
	</div>
	<div class="type1-list">
		 <div class="type1-left">
		 	<p class = 'line1'>
		 	     #苍井空#互联网卖内衣全球
				  引爆声量飙至7.9亿
		    </p>
		    <p class = 'line2'>
		    	<span class = 'love'>
		    		 <i></i>
		    		 <span>866</span>
		    	</span>
		    	<span class = 'msg'>
		    		 <span class = 't1'>12月26日 07:33:43</span>
		    		 <span class = 't2'>[课程]</span>
		    	</span>
		    </p>
		 </div> 
		 <div class="right-img">
		 	 <img src="/imgs/m12.png">
		 </div>	
	</div>
</div>



<div class="get-more">
	 <a href="#">获取更多</a>
</div>
