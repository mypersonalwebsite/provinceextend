fis.match('*.less', {
  // fis-parser-less 插件进行解析
  parser: fis.plugin('less'),
  // .less 文件后缀构建后被改成 .css 文件
  rExt: '.css'
});

// 配置配置文件，注意，清空所有的配置，只留下以下代码即可。
fis.match('*.{png,js,css}', {
  release: '/static/$0'
});

// 启用插件
fis.hook('relative');
// 让所有文件，都使用相对路径。
fis.match('**', {
  relative: true
})

// 让tpl的编译路径在跟目录下面,防止路径出错。
fis.match('*.tpl', {
  relative: '/' // 服务端访问路径
});

fis.match('*.js', {
  relative: '/', // 服务端访问路径
  optimizer: fis.plugin('uglify-js')
});

fis.match('/js/json/(*.js)', {
  optimizer:fis.plugin('') //  压缩js代码
});

fis.match('*.{js,less,css}', {
  release: '/static/$0'
});
/*
fis.match('/views/(*.html)', {
  release: '/$1'
});*/

fis.match('/imgs/(*.{png,jpg,gif,ico})', {
  release: '/static/$0'
});
fis.media('debug').match('*.{js,css,png}', {
  useHash: false,
  useSprite: false,
  optimizer: null
});

