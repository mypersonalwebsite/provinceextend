
<div class="footere-box"></div>
<div class="institution-detail">
	<div class="block1">
		<div class="title">铁人三项之软件，硬件，云</div>
		<div class="direction">
			<div class="left-direc">
				<a href="index.html">首页</a>
				<span>&gt</span>
				<a href="institution.html">GIMC营销学院</a>
			</div>
			<div class="right-time">
				2015-09-19
			</div>
		</div>
		<div class="img">
			<img src="static/imgs/i1-nav.png">
		</div>
		<div class="detail">
			<p>
			   如今，投身硬件创业的开发者越来越多。移动互联网，智能硬件设备的形态是产品，但实际上技术复杂性远远超过已经出现的PC、手机及智能设备，是软硬一体、高度整合的产品。换言之，硬件领域发生的变革并不单纯是硬件本身，而是硬件+云服务+商业模式的综合性变革。
			</p>
			<p>
				开发者最佳实践日·第4期－铁人三项专场之软件，硬件，云，七牛云存储请来了智能家居产品翘楚欧瑞博、智能图像识别与视觉交互专家亮风台、可穿戴开源硬件企业电子新我、智能语音科大讯飞、国内类gopro新秀Foream， 分享他们在产品技术打磨与创业过程中的经验，以及他们对‘软’与‘硬’结合， 打造智能软硬件生态链方面的观点。
			</p>
			<p>
				活动时间：   <br/>	
				2014年10月19日 14:00-17:30 <br/>
				活动地点： <br/>
				广州市越秀区东风东路金广大厦二楼会议室 <br/>
				活动议程： <br/> 
				13:30-14:00  签到  <br/>
				14:00-14:10  沙龙开场 <br/>
				14:10-14:30  最佳实践 欧瑞博 创始人 王雄辉 《智能家居场景化机会》 <br/>
				14:30-15:00  七牛云存储 首席布道师 徐立 《如何玩转“软件+硬件+云”铁人三项》 <br/>
				15:00-15:30  亮风台 创始人 廖春元《新一代移动图像搜索和交互平台》 <br/>
				15:30-15:40  茶歇 <br/>
				15:40-16:10  电子新我 CEO 高磊 《硬件与日常生活游戏化》 <br/>

			</p>
		</div>
	</div>
	<div class="time-address">
		 <p class="title">时间和地点</p>
		 <p class = 'time'>
		 	<i></i>
		 	<span>2014年10月19日 14:00-17:30</span>
		 </p>
		 <p class = 'address'>
		 	<i></i>
		 	<span>广州市越秀区东风东路金广大厦二楼会议室</span>
		 </p>
	</div>
	<div class="teachers">
	<div class="title">讲师简介</div>
		<ul>
			<li class = 'teacher-list'>
				<div class="portrait">
					<img src="static/imgs/portrait1.png">
				</div>
				<div class="msg">
					<p class = 'name'>谢正宁</p>
					<p class = 'position'>网络互动中心创意群总监</p>
				</div>
			</li>
			<li class = 'teacher-list'>
				<div class="portrait">
					<img src="static/imgs/portrait2.png">
				</div>
				<div class="msg">
					<p class = 'name'>谢正宁</p>
					<p class = 'position'>网络互动中心创意群总监</p>
				</div>
			</li>
			<li class = 'teacher-list'>
				<div class="portrait">
					<img src="static/imgs/portrait3.png">
				</div>
				<div class="msg">
					<p class = 'name'>谢征宇</p>
					<p class = 'position'>网络互动中心创意群总监</p>
				</div>
			</li>
		</ul>
	</div>
</div>
<div class="sign-btn">
	<a href="#">
		<span>立即报名</span>
		<i></i>
	</a>
</div>
